"use strict";

/*
1. Напишите как вы понимаете рекурсию. Для чего она используется на практике?
Рекурсия используется для повторения определенного алгоритма, до получения необходимого результата, когда функция вызывает саму себя.
При этом сильно нагружается память, рекурсию достаточно сложно писать и использовать, потому стараюсь обходиить ее по вохможностиthis.
*/

let userNum = prompt("Enter number:");

while (userNum === null || userNum === "" || isNaN(+userNum)) {
  userNum = prompt("Enter correct number!:", userNum);
}

let res = 1;

function factorial(num) {
  if (num === 0) {
    return 1;
  } else if (num === 1) {
    return res;
  } else {
    return (res = num * factorial(num - 1));
  }
}

console.log(factorial(+userNum));
